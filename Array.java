package Zadanie_3;

import java.util.Arrays;
import java.util.Scanner;

public class Array {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите количество элементов массива: ");
        int numbers = in.nextInt(); // Читаем с клавиатуры размер массива и записываем в numbers
        int array[] = new int[numbers]; // Создаём массив int размером в numbers
        System.out.println("Вводите элементы желаемого масссива: ");

        for (int i = 0; i < numbers; i++) {
            array[i] = in.nextInt(); // Заполняем массив элементами, введёнными с клавиатуры
        }
        System.out.print("То,что ввели: ");

        for (int i = 0; i < numbers; i++) {
        }
        System.out.println(Arrays.toString(array));// Выводим на экран, полученный массив
        System.out.println();


        double array_2[] = new double[numbers];
        System.out.print("Массив увеличенный на 10%: ");
        for (int i = 0; i < numbers; i++) {
          array_2[i] = array[i] * 1.1;
        }
        System.out.println(Arrays.toString(array_2));

        boolean sortirovka = false;
        while (!sortirovka) {
            sortirovka = true;
            for (int i = 0; i < numbers - 1; ++i) {
                if (array_2[i] < array_2[i + 1]) {
                    sortirovka = false;
                    double k = array_2[i];
                    array_2[i] = array_2[i + 1];
                    array_2[i + 1] = k;
                }
            }
        }
        System.out.println();
        System.out.print("Массив построеный от большего к меньшему ");
        System.out.println(Arrays.toString(array_2));
    }
}
